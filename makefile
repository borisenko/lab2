ASM=nasm
ASMFLAGS=-f elf64 -o

main: dict.asm lib.asm main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) dict.o dict.asm
	$(ASM) $(ASMFLAGS) lib.o lib.asm
	$(ASM) $(ASMFLAGS) main.o main.asm
	ld -o main dict.o lib.o main.o

clean:
	rm -f main.o lib.o dict.o main