section .text
global read_word_with_space
global read_word
global string_copy
global string_length
global print_string
global string_equals
global print_char
global print_newline

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax,60
	xor rdi,rdi
    syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax,rax
.loop:
	cmp byte[rdi+rax],0 ;сравнивает символ с 0, устанавливает флаги
	jz .end ;найден 0 -> переход к метке .end
	inc rax ;иначе увеличение счетчика
	jmp .loop ;повтор цикла
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length ;вычисление длины строки
	mov rdx, rax ;помещение длины строки в rdx
	mov rsi, rdi ;помещение указателя на строку в rsi
	mov rax, 1 ;сист вызов write
	mov rdi, 1 ;дескриптор stdout
	syscall 
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi ;помещение кода символа в стек, т.к. для вывода необходим указатель на него
	mov rsi,rsp ;указатель-адрес вершины стека
	mov rax,1
	mov rdi,1
	mov rdx,1
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi,0xA
	jmp print_char ;оптимизация хвостового вызова
	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi,0 
	jl .print_min ;если rdi меньше 0, переход в .print_min (проверка неравенства SF и OF)
	jmp .end ;иначе выводим положительное число
.print_min:
	push rdi 
	mov rdi,'-' 
	call print_char
	pop rdi
	neg rdi ;дополнительный код для rdi
.end:
	jmp print_uint ;оптимизация хвостового вызова

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10,10 ;регистр хранения числа 10 (основание сист.счисл)
	mov rax,rdi ;помещение числа в rax
	mov rdi,rsp ;сохранение адреса вершины стека
	sub rsp,21;аллоцируем место под 8-байтное беззнаковое число
	dec rdi ;перемещаем указатель для нового символа
	mov byte[rdi],0; нуль-терминируем 
.loop:
	xor rdx,rdx ; очищение rdx, куда будет попадать остаток
	div r10 ; делим rax с числом на 10, частное - в rax, остаток - в rdx. В остатке последняя цифра
	add rdx,'0' ; перевод цифры в символ
	dec rdi
	mov [rdi],dl;помещение символа в стек
	test rax,rax ;проверка, что в rax еще есть число
	jnz .loop 
.print:
	call print_string ; в rdi сохранен указатель на вершину стека
	add rsp,21; восстановление стека
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rdx,rdx ;счётчик
.equal:
	cmp byte[rdi+rdx],0 ;проверка на конец строки
	jz .next ;ZF=1 -> переход в .next
	xor al,al
	xor cl,cl
	mov al,byte[rdi+rdx] ;т.к. сравнивать память с памятью нельзя, сохранять по байту в регистры
	mov cl,byte[rsi+rdx]
	inc rdx ;увеличиваем счетчик
	cmp al,cl ; сравниваем символы
	je .equal ;если равны (ZF=1), проверяем следующие символы
	jmp .false ;иначе строки не равны, переход в .false
.next:
	cmp byte[rsi+rdx],0 ;проверка на конец строки
	jz .true ;ZF=1 -> обе строки закончены, значит, равны. Переход в .true
.false:
	xor rax,rax
	ret
.true:
	mov rax,1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	dec rsp ;смещение rsp для нового символа
	xor rax,rax
	xor rdi,rdi
	mov rdx,1
	mov rsi,rsp
	syscall
    mov rax,[rsp] 
	inc rsp ;восстановление rsp
    ret
	
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
.copy:
	xor rcx,rcx
	mov cl, [rdi] ;т.к. нельзя копировать из памяти в памяти, используем промежуточный cl
	mov [rsi], cl
	inc rsi ;cмещаем указатель на следующий символ
	inc rdi ;смещаем на след символ
	test rcx,rcx ;проверяем, что скопированный символ равен 0
	jnz .copy ;ZF=0 -> продолжаем копировать
	pop rdi
	call string_length ;иначе определяем длину строки 
	inc rax
	cmp rdx,rax ;сравниваем длину буфера и строки
	jae .end ;если длина буфера >=длина строки, то переход в .end с возвратом длины строки
	xor rax,rax ;иначе возвращаем 0
.end:
	ret
	
section .data
buffer: times 256 db 0

section .text
;аналогично read_word, но допускаются пробелы
read_word_with_space:
	mov r8,rsi ;в r8 размер буфера
	mov r9,rdi ; в r9 адрес начала буфера
	xor r10,r10 ;счётчик
	xor rdi,rdi ; заносим в rdi код 0 для считывания из stdin
	mov rdx,1 ; для чтения 1 символа
.skip:
	xor rax,rax ; код сист вызова
	mov rsi,buffer ; адрес для считывания
	syscall
	test al,al;проверка, что конец строки
	jz .finall ;если 0, то завершаем чтение
	cmp byte[buffer],0x20 ; сравниваем с кодом пробела
	je .skip  
	cmp byte[buffer],0x9 ; сравниваем с кодом табуляции
	je .skip 
	cmp byte[buffer],0xA ; сравниваем с кодом перевода строки
	je .skip 
	cmp byte[buffer],0x21 ;остальные коды считаем управляющими, при их появлении - завершаем
	jb .finall
	inc r10 ; если символ не пробельный, учитываем
.read:
	xor rax, rax 
	lea rsi,[buffer+r10] ; записываем адрес в rsi
	syscall
	mov cl,byte[buffer+r10]
	test cl,cl;проверка, что конец строки
	jz .finall ;если 0, то завершаем чтение
	cmp cl,0x20 ; сравниваем с кодом пробела
	je .write
	cmp cl,0x9 ; сравниваем с кодом табуляции
	je .write
	cmp cl,0xA ; сравниваем с кодом перевода строки
	je .write 
	cmp cl,0x21 ;сравниваем с пробельными и управляющими
	jb .finall
	cmp r8,r10 ;проверка, что символ помещается в буфер
	jbe .exi ; не помещается -> возвращаем 0
	inc r10 ; иначе учитываем символ
	jmp .read ; читаем дальше
.write:
	cmp r8,r10 ;проверка, что символ помещается в буфер
	jbe .exi ; не помещается -> возвращаем 0
	inc r10 ; иначе учитываем символ
	jmp .read ; читаем дальше
.finall:
	mov byte[buffer+r10],0 ; нультерминируем строку в буфере
	mov rdi,buffer
	mov rsi,r9
	mov rdx,r8
	call string_copy ;копируем строку из промежуточного буфера в целевой
	mov rdx,r10 ; возвращаем длину строки
	mov rax,r9 ; возвращаем указатель на буфер
	ret
.exi:
	xor rdx,r8 ;возвращаем размер буфера, т.к. вся строка не помещается
	xor rax,rax; возвращаем 0 вместо указателя
	ret

section .text
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера. 
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	mov r8,rsi ;в r8 размер буфера
	mov r9,rdi ; в r9 адрес начала буфера
	xor r10,r10 ;счётчик
	xor rdi,rdi ; заносим в rdi код 0 для считывания из stdin
	mov rdx,1 ; для чтения 1 символа
.skip:
	xor rax,rax ; код сист вызова
	mov rsi,buffer ; адрес для считывания
	syscall
	test al,al;проверка, что конец строки
	jz .finall ;если 0, то завершаем чтение
	cmp byte[buffer],0x20 ; сравниваем с кодом пробела
	je .skip  
	cmp byte[buffer],0x9 ; сравниваем с кодом табуляции
	je .skip 
	cmp byte[buffer],0xA ; сравниваем с кодом перевода строки
	je .skip 
	cmp byte[buffer],0x21 ;остальные коды считаем управляющими, при их появлении - завершаем
	jb .finall
	inc r10 ; если символ не пробельный, учитываем
.read:
	xor rax, rax 
	lea rsi,[buffer+r10] ; записываем адрес в rsi
	syscall
	mov cl,byte[buffer+r10]
	cmp cl,0x21 ;сравниваем с пробельными и управляющими
	jb .finall
	cmp r8,r10 ;проверка, что символ помещается в буфер
	jbe .exi ; не помещается -> возвращаем 0
	inc r10 ; иначе учитываем символ
	jmp .read ; читаем дальше
.finall:
	mov byte[buffer+r10],0 ; нультерминируем строку в буфере
	mov rdi,buffer
	mov rsi,r9
	mov rdx,r8
	call string_copy ;копируем строку из промежуточного буфера в целевой
	mov rdx,r10 ; возвращаем длину строки
	mov rax,r9 ; возвращаем указатель на буфер
	ret
.exi:
	xor rdx,r8 ;возвращаем размер буфера, т.к. вся строка не помещается
	xor rax,rax; возвращаем 0 вместо указателя
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rsi,rsi ;счётчик длины строки
	xor rax,rax ;регистр для числа
	xor r9,r9 ;регистр для копирования символов
	mov r8,10 ;основание системы счисления
.numbers:
	mov r9b,byte[rdi+rsi] ;копируем символ
    cmp r9b, '0' ;проверяем, что это цифра
    jb .end 
    cmp r9b, '9'
    ja .end
	mul r8 ; умножаем содержимое rax на r8 для перевода в сс
    sub r9b,'0' ;перевод символа в цифру
    add rax,r9 ;добавляем цифру в rax
    inc rsi ;увеличиваем счетчик
    jmp .numbers
.end:
    mov rdx,rsi
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены. 0x20, табуляция 0x9 и перевод строки 0x10
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rsi,rsi
	xor rdx,rdx
	mov r9b,byte[rdi]
    cmp r9b, '-' ;проверяем, является ли число отрицательным
    je .minus
	call parse_uint ;нет -> можно преобразовать как беззнаковое
	ret
.minus:
	inc rdi
	mov r9b,byte[rdi+rsi]
	cmp r9b,0x20 ;проверка наличия пробелов между знаком и числом
	je .end
	cmp r9b,0x9
	je .end
	cmp r9b,0x10
	je .end
	call parse_uint 
	test rdx,rdx ;проверка, что после знака действительно идёт число
	jz .end 
	inc rdx ;увеличение счетчика из-за знака
	neg rax ;преобразование числа в отрицательное
	ret 
.end:
	ret