global find_word
extern string_equals

section .text
find_word:
	test rsi,rsi ;если rsi=0,то все ключи проверены и совпадений нет
	jz .end 
	push rsi ;сохраняем адрес следующего слова
	add rsi,8 ;добавляем 8 байт к адресу (т.к. адрес след слова занимает 8 байт), чтобы получить адрес ключа
	call string_equals ;сравниваем ключ и запрашиваемый ключ
	pop rsi 
	test rax,rax ;string_equals возвращает 1, если строки равны, 0, если не равны
	jnz .success ;не 0, значит, ключ найден
	mov rsi,[rsi] ;иначе переходит к следующему адресу
	jmp find_word
.end:
	xor rax,rax ; 0 - не найден ключ
	ret
.success:
	mov rax,rsi ; указатель на слово
	ret
	