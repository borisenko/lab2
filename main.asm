extern read_word_with_space
extern read_word
extern string_copy
extern string_length
extern find_word
extern print_string
extern print_char
extern print_newline
global _start

section .data
key_error_message: db "This key ins't in dictionary.",0
buffer: times 256 db 0

section .text
%include "colon.inc"
%include "words.inc"

_start:
	mov rdi,buffer ;куда считаем слово для поиска
	mov rsi,256 ;размер буфера
	call read_word_with_space 
	push rdx 
	mov rdi,rax
	mov rsi,pointer ;в colon.inc определяется pointer на последнее слово
	call find_word
	test rax,rax ;0 - ключ не найден
	jz .error
	add rax,8 ;переходим к ключу
	push rax 
	mov rdi,rax 
	call string_length ;определяем длину ключа
	push rax 
	call print_newline
	pop rax
	pop rdi
	inc rdi ;увеличиваем rdi (адрес ключа) на 1 из-за нуль-терминирования
	add rdi,rax ;увеличиваем адрес на длину ключа
	call print_string ;выводим пояснения к слову
	call print_newline
.end:
	mov rax,60
	xor rdi,rdi
	syscall
.error:
	mov rdi,key_error_message
	call string_length
	mov rdx,rax
	mov rax,1 ;сист вызов write
	mov rdi,2 ;дескриптор stderror
	mov rsi,key_error_message
	syscall
	jmp .end